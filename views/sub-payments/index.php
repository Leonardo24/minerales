<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-payments-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Sub Payments', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'Quota-ID',
            'PaymentRef',
            'PaymentDate',
            'Amount',
            // 'Type',
            // 'Comment',
            // 'PayementType',
            // 'DueDate',
            // 'InvoiceNumber',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
