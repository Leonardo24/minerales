<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LookupLocation */

$this->title = 'Create Lookup Location';
$this->params['breadcrumbs'][] = ['label' => 'Lookup Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-location-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
