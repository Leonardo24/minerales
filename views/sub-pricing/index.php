<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Pricings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-pricing-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Sub Pricing', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'Quota-ID',
            'Element',
            'PricingDate',
            'Price',
            // 'Quantity',
            // 'Type',
            // 'Measurement',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
