<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LookupCustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lookup Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-customers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Lookup Customers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'CustomerName',
            'SupplierConsumer',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
