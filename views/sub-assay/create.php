<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubAssay */

$this->title = 'Create Sub Assay';
$this->params['breadcrumbs'][] = ['label' => 'Sub Assays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-assay-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
