<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LookupBanksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lookup Banks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-banks-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Lookup Banks', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'FinancingBank',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
