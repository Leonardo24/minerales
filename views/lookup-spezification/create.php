<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LookupSpezification */

$this->title = 'Create Lookup Spezification';
$this->params['breadcrumbs'][] = ['label' => 'Lookup Spezifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-spezification-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
