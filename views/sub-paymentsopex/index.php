<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubPaymentsopexSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Paymentsopexes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-paymentsopex-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sub Paymentsopex', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'Quota-ID',
            'PaymentRef',
            'PaymentDate',
            'Amount',
            // 'Type',
            // 'Comment',
            // 'PayementType',
            // 'DueDate',
            // 'InvoiceNumber',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
