<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubBenefitcostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Benefitcosts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-benefitcost-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sub Benefitcost', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Quota-ID',
            'BenefitCost',
            'AmountWMT',
            'AmountDMT',
            'CalculateDMT',
            // 'GetFromContract',
            // 'NotContractLinkedCosts',
            // 'ID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
