<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubLoanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Loans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-loan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sub Loan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'ContractID',
            'RepaymentDate',
            'Principal',
            'Interest',
            // 'Value Date',
            // 'AmountPaid',
            // 'InvoiceNo',
            // 'Comment',
            // 'PaymentType',
            // 'Type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
