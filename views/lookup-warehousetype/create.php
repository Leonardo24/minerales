<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LookupWarehousetype */

$this->title = 'Create Lookup Warehousetype';
$this->params['breadcrumbs'][] = ['label' => 'Lookup Warehousetypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-warehousetype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
