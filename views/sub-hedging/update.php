<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubHedging */

$this->title = 'Update Sub Hedging: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Sub Hedgings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sub-hedging-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
