<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubAllocationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-allocation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Quota-ID') ?>

    <?= $form->field($model, 'Quota-IDPurchase') ?>

    <?= $form->field($model, 'LotNumber') ?>

    <?= $form->field($model, 'Contract') ?>

    <?= $form->field($model, 'Supplier') ?>

    <?php // echo $form->field($model, 'DeliveryDate') ?>

    <?php // echo $form->field($model, 'WMT') ?>

    <?php // echo $form->field($model, 'DMT') ?>

    <?php // echo $form->field($model, 'DMTPayable') ?>

    <?php // echo $form->field($model, 'Delivered') ?>

    <?php // echo $form->field($model, 'MaterialValue') ?>

    <?php // echo $form->field($model, 'Comment') ?>

    <?php // echo $form->field($model, 'Pricing-ID') ?>

    <?php // echo $form->field($model, 'LatestPricingDate') ?>

    <?php // echo $form->field($model, 'Priced') ?>

    <?php // echo $form->field($model, 'Paid') ?>

    <?php // echo $form->field($model, 'DispatchIDPurchase') ?>

    <?php // echo $form->field($model, 'ID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
