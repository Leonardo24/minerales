<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubAllocationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Allocations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-allocation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sub Allocation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Quota-ID',
            'Quota-IDPurchase',
            'LotNumber',
            'Contract',
            'Supplier',
            // 'DeliveryDate',
            // 'WMT',
            // 'DMT',
            // 'DMTPayable',
            // 'Delivered',
            // 'MaterialValue',
            // 'Comment:ntext',
            // 'Pricing-ID:ntext',
            // 'LatestPricingDate',
            // 'Priced',
            // 'Paid',
            // 'DispatchIDPurchase',
            // 'ID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
