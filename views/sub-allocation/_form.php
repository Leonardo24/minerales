<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubAllocation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-allocation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Quota-ID')->textInput() ?>

    <?= $form->field($model, 'Quota-IDPurchase')->textInput() ?>

    <?= $form->field($model, 'LotNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Contract')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Supplier')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DeliveryDate')->textInput() ?>

    <?= $form->field($model, 'WMT')->textInput() ?>

    <?= $form->field($model, 'DMT')->textInput() ?>

    <?= $form->field($model, 'DMTPayable')->textInput() ?>

    <?= $form->field($model, 'Delivered')->textInput() ?>

    <?= $form->field($model, 'MaterialValue')->textInput() ?>

    <?= $form->field($model, 'Comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Pricing-ID')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'LatestPricingDate')->textInput() ?>

    <?= $form->field($model, 'Priced')->textInput() ?>

    <?= $form->field($model, 'Paid')->textInput() ?>

    <?= $form->field($model, 'DispatchIDPurchase')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
