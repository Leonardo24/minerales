<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubPaymentsopex;

/**
 * SubPaymentsopexSearch represents the model behind the search form about `app\models\SubPaymentsopex`.
 */
class SubPaymentsopexSearch extends SubPaymentsopex
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Quota-ID', 'Type', 'InvoiceNumber'], 'integer'],
            [['PaymentRef', 'PaymentDate', 'Comment', 'PayementType', 'DueDate'], 'safe'],
            [['Amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubPaymentsopex::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Quota-ID' => $this->Quota-ID,
            'PaymentDate' => $this->PaymentDate,
            'Amount' => $this->Amount,
            'Type' => $this->Type,
            'DueDate' => $this->DueDate,
            'InvoiceNumber' => $this->InvoiceNumber,
        ]);

        $query->andFilterWhere(['like', 'PaymentRef', $this->PaymentRef])
            ->andFilterWhere(['like', 'Comment', $this->Comment])
            ->andFilterWhere(['like', 'PayementType', $this->PayementType]);

        return $dataProvider;
    }
}
