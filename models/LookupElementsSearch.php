<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LookupElements;

/**
 * LookupElementsSearch represents the model behind the search form about `app\models\LookupElements`.
 */
class LookupElementsSearch extends LookupElements
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'integer'],
            [['Elements', 'Description', 'PayableCalcVersion', 'Measurement', 'Unit'], 'safe'],
            [['PayableBorder', 'MarketPrice'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LookupElements::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'PayableBorder' => $this->PayableBorder,
            'MarketPrice' => $this->MarketPrice,
        ]);

        $query->andFilterWhere(['like', 'Elements', $this->Elements])
            ->andFilterWhere(['like', 'Description', $this->Description])
            ->andFilterWhere(['like', 'PayableCalcVersion', $this->PayableCalcVersion])
            ->andFilterWhere(['like', 'Measurement', $this->Measurement])
            ->andFilterWhere(['like', 'Unit', $this->Unit]);

        return $dataProvider;
    }
}
