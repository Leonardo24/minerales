<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_pricing".
 *
 * @property integer $ID
 * @property integer $Quota-ID
 * @property integer $Element
 * @property string $PricingDate
 * @property double $Price
 * @property double $Quantity
 * @property integer $Type
 * @property string $Measurement
 *
 * @property LookupElements $element
 * @property LookupPricingtype $type
 * @property MainQuota $quota-
 */
class SubPricing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_pricing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Quota-ID', 'Element', 'Type'], 'integer'],
            [['Element'], 'required'],
            [['PricingDate'], 'safe'],
            [['Price', 'Quantity'], 'number'],
            [['Measurement'], 'string', 'max' => 255],
            [['Element'], 'exist', 'skipOnError' => true, 'targetClass' => LookupElements::className(), 'targetAttribute' => ['Element' => 'ID']],
            [['Type'], 'exist', 'skipOnError' => true, 'targetClass' => LookupPricingtype::className(), 'targetAttribute' => ['Type' => 'ID']],
            [['Quota-ID'], 'exist', 'skipOnError' => true, 'targetClass' => MainQuota::className(), 'targetAttribute' => ['Quota-ID' => 'Quota-ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Quota-ID' => 'Quota  ID',
            'Element' => 'Element',
            'PricingDate' => 'Pricing Date',
            'Price' => 'Price',
            'Quantity' => 'Quantity',
            'Type' => 'Type',
            'Measurement' => 'Measurement',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElement()
    {
        return $this->hasOne(LookupElements::className(), ['ID' => 'Element']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(LookupPricingtype::className(), ['ID' => 'Type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuota()
    {
        return $this->hasOne(MainQuota::className(), ['Quota-ID' => 'Quota-ID']);
    }
}
