<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_payabledetails".
 *
 * @property integer $ID
 * @property integer $ContractID
 * @property integer $Element
 * @property double $Payable
 * @property double $MinContent
 * @property double $MaxContent
 * @property string $Measurement
 *
 * @property MainContract $contract
 */
class SubPayabledetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_payabledetails';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ContractID', 'Element'], 'integer'],
            [['Payable', 'MinContent', 'MaxContent'], 'number'],
            [['Measurement'], 'string', 'max' => 255],
            [['ContractID'], 'exist', 'skipOnError' => true, 'targetClass' => MainContract::className(), 'targetAttribute' => ['ContractID' => 'Contract-ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ContractID' => 'Contract ID',
            'Element' => 'Element',
            'Payable' => 'Payable',
            'MinContent' => 'Min Content',
            'MaxContent' => 'Max Content',
            'Measurement' => 'Measurement',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(MainContract::className(), ['Contract-ID' => 'ContractID']);
    }
}
