<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_payments".
 *
 * @property integer $ID
 * @property integer $Quota-ID
 * @property string $PaymentRef
 * @property string $PaymentDate
 * @property double $Amount
 * @property integer $Type
 * @property string $Comment
 * @property string $PayementType
 * @property string $DueDate
 * @property integer $InvoiceNumber
 *
 * @property LookupPaymenttype $type
 * @property MainQuota $quota-
 */
class SubPayments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Quota-ID', 'Type', 'InvoiceNumber'], 'integer'],
            [['PaymentDate', 'DueDate'], 'safe'],
            [['Amount'], 'number'],
            [['PaymentRef', 'Comment', 'PayementType'], 'string', 'max' => 255],
            [['Type'], 'exist', 'skipOnError' => true, 'targetClass' => LookupPaymenttype::className(), 'targetAttribute' => ['Type' => 'ID']],
            [['Quota-ID'], 'exist', 'skipOnError' => true, 'targetClass' => MainQuota::className(), 'targetAttribute' => ['Quota-ID' => 'Quota-ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Quota-ID' => 'Quota  ID',
            'PaymentRef' => 'Payment Ref',
            'PaymentDate' => 'Payment Date',
            'Amount' => 'Amount',
            'Type' => 'Type',
            'Comment' => 'Comment',
            'PayementType' => 'Payement Type',
            'DueDate' => 'Due Date',
            'InvoiceNumber' => 'Invoice Number',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(LookupPaymenttype::className(), ['ID' => 'Type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuota()
    {
        return $this->hasOne(MainQuota::className(), ['Quota-ID' => 'Quota-ID']);
    }
}
