<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_dispatch".
 *
 * @property integer $ID
 * @property integer $Quota-ID
 * @property string $TruckPlate
 * @property string $Departure
 * @property integer $DepartureETA
 * @property integer $DepartureLocation
 * @property string $ArrivalOruro
 * @property integer $ArrivalOruroETA
 * @property string $Arrival
 * @property integer $ArrivalETA
 * @property integer $ArrivalLocation
 * @property double $WMT
 * @property double $DMT
 * @property double $DMTPayable
 * @property integer $Document
 * @property double $MaterialValue
 * @property integer $Control
 * @property string $Comment
 * @property string $Pricing-ID
 * @property string $LatestPricingDate
 * @property integer $Priced
 * @property integer $WarehouseType
 * @property integer $Delivered
 * @property string $Transport
 * @property integer $NoOfTrucks
 *
 * @property LookupDocument $document
 * @property LookupWarehousetype $warehouseType
 * @property MainQuota $quota-
 */
class SubDispatch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_dispatch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Quota-ID', 'DepartureETA', 'DepartureLocation', 'ArrivalOruroETA', 'ArrivalETA', 'ArrivalLocation', 'Document', 'Control', 'Priced', 'WarehouseType', 'Delivered', 'NoOfTrucks'], 'integer'],
            [['Departure', 'ArrivalOruro', 'Arrival', 'LatestPricingDate'], 'safe'],
            [['DepartureETA', 'ArrivalOruroETA', 'ArrivalETA', 'Document', 'Control', 'Priced', 'Delivered'], 'required'],
            [['WMT', 'DMT', 'DMTPayable', 'MaterialValue'], 'number'],
            [['Comment', 'Pricing-ID'], 'string'],
            [['TruckPlate'], 'string', 'max' => 25],
            [['Transport'], 'string', 'max' => 255],
            [['Document'], 'exist', 'skipOnError' => true, 'targetClass' => LookupDocument::className(), 'targetAttribute' => ['Document' => 'ID']],
            [['WarehouseType'], 'exist', 'skipOnError' => true, 'targetClass' => LookupWarehousetype::className(), 'targetAttribute' => ['WarehouseType' => 'ID']],
            [['Quota-ID'], 'exist', 'skipOnError' => true, 'targetClass' => MainQuota::className(), 'targetAttribute' => ['Quota-ID' => 'Quota-ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Quota-ID' => 'Quota  ID',
            'TruckPlate' => 'Truck Plate',
            'Departure' => 'Departure',
            'DepartureETA' => 'Departure Eta',
            'DepartureLocation' => 'Departure Location',
            'ArrivalOruro' => 'Arrival Oruro',
            'ArrivalOruroETA' => 'Arrival Oruro Eta',
            'Arrival' => 'Arrival',
            'ArrivalETA' => 'Arrival Eta',
            'ArrivalLocation' => 'Arrival Location',
            'WMT' => 'Wmt',
            'DMT' => 'Dmt',
            'DMTPayable' => 'Dmtpayable',
            'Document' => 'Document',
            'MaterialValue' => 'Material Value',
            'Control' => 'Control',
            'Comment' => 'Comment',
            'Pricing-ID' => 'Pricing  ID',
            'LatestPricingDate' => 'Latest Pricing Date',
            'Priced' => 'Priced',
            'WarehouseType' => 'Warehouse Type',
            'Delivered' => 'Delivered',
            'Transport' => 'Transport',
            'NoOfTrucks' => 'No Of Trucks',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(LookupDocument::className(), ['ID' => 'Document']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseType()
    {
        return $this->hasOne(LookupWarehousetype::className(), ['ID' => 'WarehouseType']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuota()
    {
        return $this->hasOne(MainQuota::className(), ['Quota-ID' => 'Quota-ID']);
    }
}
