<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_location".
 *
 * @property integer $ID
 * @property string $Location
 */
class LookupLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Location'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Location' => 'Location',
        ];
    }
}
