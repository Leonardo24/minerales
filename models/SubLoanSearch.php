<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubLoan;

/**
 * SubLoanSearch represents the model behind the search form about `app\models\SubLoan`.
 */
class SubLoanSearch extends SubLoan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ContractID'], 'integer'],
            [['RepaymentDate', 'Value Date', 'InvoiceNo', 'Comment', 'PaymentType', 'Type'], 'safe'],
            [['Principal', 'Interest', 'AmountPaid'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubLoan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'ContractID' => $this->ContractID,
            'RepaymentDate' => $this->RepaymentDate,
            'Principal' => $this->Principal,
            'Interest' => $this->Interest,
            'Value Date' => $this->Value Date,
            'AmountPaid' => $this->AmountPaid,
        ]);

        $query->andFilterWhere(['like', 'InvoiceNo', $this->InvoiceNo])
            ->andFilterWhere(['like', 'Comment', $this->Comment])
            ->andFilterWhere(['like', 'PaymentType', $this->PaymentType])
            ->andFilterWhere(['like', 'Type', $this->Type]);

        return $dataProvider;
    }
}
