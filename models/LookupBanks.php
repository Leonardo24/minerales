<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_banks".
 *
 * @property integer $ID
 * @property string $FinancingBank
 *
 * @property MainQuota[] $mainQuotas
 */
class LookupBanks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_banks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FinancingBank'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'FinancingBank' => 'Financing Bank',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainQuotas()
    {
        return $this->hasMany(MainQuota::className(), ['FinancingBank' => 'ID']);
    }
}
