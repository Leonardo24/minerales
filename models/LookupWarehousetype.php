<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_warehousetype".
 *
 * @property integer $ID
 * @property string $WHType
 *
 * @property SubDispatch[] $subDispatches
 */
class LookupWarehousetype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_warehousetype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['WHType'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'WHType' => 'Whtype',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubDispatches()
    {
        return $this->hasMany(SubDispatch::className(), ['WarehouseType' => 'ID']);
    }
}
