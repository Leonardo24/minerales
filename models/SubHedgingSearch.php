<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubHedging;

/**
 * SubHedgingSearch represents the model behind the search form about `app\models\SubHedging`.
 */
class SubHedgingSearch extends SubHedging
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TradeDate', 'PromptDate', 'Unit', 'BrokerRef'], 'safe'],
            [['Element', 'Quota-ID', 'ID'], 'integer'],
            [['Quantity', 'Price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubHedging::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'TradeDate' => $this->TradeDate,
            'PromptDate' => $this->PromptDate,
            'Element' => $this->Element,
            'Quantity' => $this->Quantity,
            'Price' => $this->Price,
            'Quota-ID' => $this->Quota-ID,
            'ID' => $this->ID,
        ]);

        $query->andFilterWhere(['like', 'Unit', $this->Unit])
            ->andFilterWhere(['like', 'BrokerRef', $this->BrokerRef]);

        return $dataProvider;
    }
}
