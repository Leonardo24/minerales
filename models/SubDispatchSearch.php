<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubDispatch;

/**
 * SubDispatchSearch represents the model behind the search form about `app\models\SubDispatch`.
 */
class SubDispatchSearch extends SubDispatch
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Quota-ID', 'DepartureETA', 'DepartureLocation', 'ArrivalOruroETA', 'ArrivalETA', 'ArrivalLocation', 'Document', 'Control', 'Priced', 'WarehouseType', 'Delivered', 'NoOfTrucks'], 'integer'],
            [['TruckPlate', 'Departure', 'ArrivalOruro', 'Arrival', 'Comment', 'Pricing-ID', 'LatestPricingDate', 'Transport'], 'safe'],
            [['WMT', 'DMT', 'DMTPayable', 'MaterialValue'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubDispatch::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Quota-ID' => $this->Quota-ID,
            'Departure' => $this->Departure,
            'DepartureETA' => $this->DepartureETA,
            'DepartureLocation' => $this->DepartureLocation,
            'ArrivalOruro' => $this->ArrivalOruro,
            'ArrivalOruroETA' => $this->ArrivalOruroETA,
            'Arrival' => $this->Arrival,
            'ArrivalETA' => $this->ArrivalETA,
            'ArrivalLocation' => $this->ArrivalLocation,
            'WMT' => $this->WMT,
            'DMT' => $this->DMT,
            'DMTPayable' => $this->DMTPayable,
            'Document' => $this->Document,
            'MaterialValue' => $this->MaterialValue,
            'Control' => $this->Control,
            'LatestPricingDate' => $this->LatestPricingDate,
            'Priced' => $this->Priced,
            'WarehouseType' => $this->WarehouseType,
            'Delivered' => $this->Delivered,
            'NoOfTrucks' => $this->NoOfTrucks,
        ]);

        $query->andFilterWhere(['like', 'TruckPlate', $this->TruckPlate])
            ->andFilterWhere(['like', 'Comment', $this->Comment])
            ->andFilterWhere(['like', 'Pricing-ID', $this->Pricing-ID])
            ->andFilterWhere(['like', 'Transport', $this->Transport]);

        return $dataProvider;
    }
}
