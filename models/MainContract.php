<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "main_contract".
 *
 * @property integer $Contract-ID
 * @property string $PS
 * @property integer $Counterparty
 * @property integer $Material
 * @property string $ContractualAssays
 * @property integer $Tolerance
 * @property string $ContractDate
 * @property string $ContractNumber
 * @property double $TotalContractualBaseMt
 * @property integer $Delivery
 * @property string $Comment
 * @property integer $NumberOfQuotas
 * @property double $Franchise
 * @property string $Payment
 * @property double $ProvisionalPayment
 * @property double $FinalPayment
 * @property integer $Finalized
 * @property integer $Spezification
 * @property string $CustomerRef
 * @property double $ContractualMoisture
 * @property double $Loan
 * @property integer $ContractNumberFix
 *
 * @property LookupCustomers $counterparty
 * @property LookupMaterial $material
 * @property LookupSpezification $spezification
 * @property MainQuota[] $mainQuotas
 * @property SubBenefits[] $subBenefits
 * @property SubContractualessays[] $subContractualessays
 * @property SubLoan[] $subLoans
 * @property SubPayabledetails[] $subPayabledetails
 * @property SubPenalties[] $subPenalties
 * @property SubTolerance[] $subTolerances
 */
class MainContract extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_contract';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Counterparty', 'Material', 'Tolerance', 'Delivery', 'NumberOfQuotas', 'Finalized', 'Spezification', 'ContractNumberFix'], 'integer'],
            [['ContractualAssays', 'Comment'], 'string'],
            [['ContractDate', 'TotalContractualBaseMt', 'Delivery', 'NumberOfQuotas', 'Finalized', 'ContractNumberFix'], 'required'],
            [['ContractDate'], 'safe'],
            [['TotalContractualBaseMt', 'Franchise', 'ProvisionalPayment', 'FinalPayment', 'ContractualMoisture', 'Loan'], 'number'],
            [['PS', 'ContractNumber', 'Payment', 'CustomerRef'], 'string', 'max' => 255],
            [['Counterparty'], 'exist', 'skipOnError' => true, 'targetClass' => LookupCustomers::className(), 'targetAttribute' => ['Counterparty' => 'ID']],
            [['Material'], 'exist', 'skipOnError' => true, 'targetClass' => LookupMaterial::className(), 'targetAttribute' => ['Material' => 'ID']],
            [['Spezification'], 'exist', 'skipOnError' => true, 'targetClass' => LookupSpezification::className(), 'targetAttribute' => ['Spezification' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Contract-ID' => 'Contract  ID',
            'PS' => 'Ps',
            'Counterparty' => 'Counterparty',
            'Material' => 'Material',
            'ContractualAssays' => 'Contractual Assays',
            'Tolerance' => 'Tolerance',
            'ContractDate' => 'Contract Date',
            'ContractNumber' => 'Contract Number',
            'TotalContractualBaseMt' => 'Total Contractual Base Mt',
            'Delivery' => 'Delivery',
            'Comment' => 'Comment',
            'NumberOfQuotas' => 'Number Of Quotas',
            'Franchise' => 'Franchise',
            'Payment' => 'Payment',
            'ProvisionalPayment' => 'Provisional Payment',
            'FinalPayment' => 'Final Payment',
            'Finalized' => 'Finalized',
            'Spezification' => 'Spezification',
            'CustomerRef' => 'Customer Ref',
            'ContractualMoisture' => 'Contractual Moisture',
            'Loan' => 'Loan',
            'ContractNumberFix' => 'Contract Number Fix',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounterparty()
    {
        return $this->hasOne(LookupCustomers::className(), ['ID' => 'Counterparty']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterial()
    {
        return $this->hasOne(LookupMaterial::className(), ['ID' => 'Material']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpezification()
    {
        return $this->hasOne(LookupSpezification::className(), ['ID' => 'Spezification']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainQuotas()
    {
        return $this->hasMany(MainQuota::className(), ['Contract-ID' => 'Contract-ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubBenefits()
    {
        return $this->hasMany(SubBenefits::className(), ['Contract-ID' => 'Contract-ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubContractualessays()
    {
        return $this->hasMany(SubContractualessays::className(), ['ContractID' => 'Contract-ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubLoans()
    {
        return $this->hasMany(SubLoan::className(), ['ContractID' => 'Contract-ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubPayabledetails()
    {
        return $this->hasMany(SubPayabledetails::className(), ['ContractID' => 'Contract-ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubPenalties()
    {
        return $this->hasMany(SubPenalties::className(), ['Contract-ID' => 'Contract-ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubTolerances()
    {
        return $this->hasMany(SubTolerance::className(), ['ContractID' => 'Contract-ID']);
    }
}
