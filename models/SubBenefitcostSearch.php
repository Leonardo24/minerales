<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubBenefitcost;

/**
 * SubBenefitcostSearch represents the model behind the search form about `app\models\SubBenefitcost`.
 */
class SubBenefitcostSearch extends SubBenefitcost
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Quota-ID', 'BenefitCost', 'CalculateDMT', 'GetFromContract', 'NotContractLinkedCosts', 'ID'], 'integer'],
            [['AmountWMT', 'AmountDMT'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubBenefitcost::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Quota-ID' => $this->Quota-ID,
            'BenefitCost' => $this->BenefitCost,
            'AmountWMT' => $this->AmountWMT,
            'AmountDMT' => $this->AmountDMT,
            'CalculateDMT' => $this->CalculateDMT,
            'GetFromContract' => $this->GetFromContract,
            'NotContractLinkedCosts' => $this->NotContractLinkedCosts,
            'ID' => $this->ID,
        ]);

        return $dataProvider;
    }
}
